const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const VENDOR_LIBS = [
    "faker",
    "lodash",
    "react",
    "react-dom",
    "react-input-range",
    "react-redux",
    "react-router",
    "redux",
    "redux-form",
    "redux-thunk"
];

module.exports = {
    entry: {
        bundle: './src/index.js',
        vendor: VENDOR_LIBS
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[chunkhash].js'
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.jsx?$/,
                exclude: /node_modules/
            },
            {
                use: ['style-loader', 'css-loader'],
                test: /\.css$/
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({ // wtyka tworzy osobny plik js: vendor, z libsami
            name: ['vendor', 'manifest'] // manifest daje informacje czy vendor się zmienił
            // rimraf czysci folder dist. scrypt umieszczamy np w packed.json scripts
        }),
        new HtmlWebpackPlugin({ // wtyka dodaje to wskazanego index.html wymagane scrypty
            template: 'src/index.html'
        }),
        new webpack.DefinePlugin({ // Ustawia ustawienia, dzięki którym react nie będzie wykonywać pewnych testów kodu. (ustawienia produkcyjne)
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            // zmiana również w package.json : NODE_ENV=production + "-p"
        })
    ]
};
